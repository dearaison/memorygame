package controller;

import model.Model;

public class GameBoardController implements IController {
	private Model model;

	/**
	 * @param panel
	 * @param model
	 */
	public GameBoardController(Model model) {
		super();
		this.model = model;
	}

	public void changeState(int numZone) {
		this.model.modifier(numZone);
	}

	@Override
	public void rePlay() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int calculateScore() {
		// TODO Auto-generated method stub
		return 0;
	}

}
