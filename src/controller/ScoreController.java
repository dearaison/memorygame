package controller;

import model.Model;

public class ScoreController implements IController {
	private Model model;

	/**
	 * @param model
	 */
	public ScoreController(Model model) {
		super();
		this.model = model;
	}

	@Override
	public void changeState(int numZone) {
		// TODO Auto-generated method stub

	}

	@Override
	public void rePlay() {
		// TODO Auto-generated method stub

	}

	@Override
	public int calculateScore() {
		return this.model.calculateScore();
	}

}
