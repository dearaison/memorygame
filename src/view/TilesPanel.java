package view;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;

import controller.IController;
import model.GameBoard;
import model.adapter.IAdapter;
import model.tiles.imagetiles.ImageTiles;

@SuppressWarnings("serial")
public class TilesPanel extends JPanel implements Observer, MouseListener {
	private IAdapter adapter;
	private IController controller;

	/**
	 * @param adapter
	 * @param controller
	 */
	public TilesPanel(IAdapter adapter, IController controller) {
		super();
		this.adapter = adapter;
		this.controller = controller;
		this.adapter.getObserableGameBoard().addObserver(this);
		this.addMouseListener(this);
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		setBackground(Color.white);
		int x = 5, y = 5;

		int tilesListSize = this.adapter.getTilesListSize();

		for (int i = 0; i < tilesListSize; i++) {
			this.adapter.getImageTile(i).drawImageTile(g, x, y, this);
			x += ImageTiles.TILE_WIDTH;
			if (x == 5 + ImageTiles.TILE_WIDTH * GameBoard.NUMBER_OF_TILES_ON_ROW) {
				x = 5;
				y += ImageTiles.TILE_HEIGHT;
			}
		}
	}

	@Override
	public void update(Observable o, Object arg) {
		repaint();
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {

		// get the mouse pressed coordinate.
		int x = e.getX();
		int y = e.getY();

		// The reality size of one tile is width = 115px height = 115px.

		// So that the index of tile base on x coordinate is x / width, but we
		// want to make sure the accuracy of mouse click. The index is x/120.

		// And, the index base on y coordinate is y/height, for the accuracy
		// goal as above, the index will be y/120. But you can see when the y
		// index is 0, the tile index in the tiles list is 5; when the y index
		// is 2 the tile index in the tiles list is 10 and so on, so that we
		// multiply the y index by 5.

		// Finally, the sum of x index and y index is the tile index in the
		// tiles list.
		int positionImage = (int) Math.floor(x / 120) + (int) Math.floor(y / 120) * 5;
		this.controller.changeState(positionImage);
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

}
