package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

import model.GameBoard;
import model.tiles.imagetiles.ImageTiles;

@SuppressWarnings("serial")
public class GameFrame extends JFrame {
	private JPanel secondaryPanel, controlPanel;
	private Border compound;
	
	/**
	 * @param tilesPanel
	 * @param scoreLable
	 * @param replayButton
	 */
	public GameFrame(JPanel tilesPanel, JLabel scoreLable, JButton replayButton) throws HeadlessException {
		super();
		// create border
		Border raisedBevel = BorderFactory.createRaisedBevelBorder();
		Border loweredBevel = BorderFactory.createLoweredBevelBorder();
		compound = BorderFactory.createCompoundBorder(raisedBevel, loweredBevel);

		secondaryPanel = new JPanel(new GridLayout(2, 1));

		scoreLable.setPreferredSize(new Dimension(5 + ImageTiles.TILE_WIDTH * GameBoard.NUMBER_OF_TILES_ON_ROW, 55));
		scoreLable.setOpaque(true);
		scoreLable.setForeground(Color.blue);
		scoreLable.setBorder(compound);
		secondaryPanel.add(scoreLable);

		controlPanel = new JPanel();
		controlPanel.add(replayButton);
		secondaryPanel.add(controlPanel);

		tilesPanel.setPreferredSize(new Dimension(5 + ImageTiles.TILE_WIDTH * GameBoard.NUMBER_OF_TILES_ON_ROW,
				5 + ImageTiles.TILE_HEIGHT * GameBoard.NUMBER_OF_TILES_ON_COLUMN));
		tilesPanel.setBorder(compound);

		Container containPane = this.getContentPane();
		containPane.setBackground(Color.BLUE);

		containPane.add(tilesPanel, BorderLayout.CENTER);
		containPane.add(secondaryPanel, BorderLayout.SOUTH);

		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.pack();
		this.setVisible(true);
		this.setLocationRelativeTo(null);

	}
}
