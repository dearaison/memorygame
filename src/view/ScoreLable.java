package view;

import java.util.Observable;
import java.util.Observer;

import javax.swing.JLabel;

import controller.IController;
import model.adapter.IAdapter;

@SuppressWarnings("serial")
public class ScoreLable extends JLabel implements Observer {
	private IController controller;
	private IAdapter adapter;

	/**
	 * @param controller
	 */
	public ScoreLable(IController controller, IAdapter adapter) {
		super();
		this.controller = controller;
		this.adapter = adapter;
		this.adapter.getObserableGameBoard().addObserver(this);
		this.setText("Your score: 0");
	}

	@Override
	public void update(Observable o, Object arg1) {
		this.setText("Your score: " + this.controller.calculateScore());
	}

}
