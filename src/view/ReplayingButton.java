package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import controller.IController;

@SuppressWarnings("serial")
public class ReplayingButton extends JButton implements ActionListener {
	IController controller;

	/**
	 * @param controller
	 */
	public ReplayingButton(IController controller) {
		super();
		this.controller = controller;
		this.setText("Replay");
		this.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		this.controller.rePlay();
	}
}
