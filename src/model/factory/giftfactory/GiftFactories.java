package model.factory.giftfactory;

import model.tiles.gifttiles.Gifts;
import model.tiles.imagetiles.ImageTiles;

public abstract class GiftFactories {
	protected int minIdGiftTile;
	protected int maxIdGiftTile;

	public abstract Gifts createGiftTile(int giftTileID, ImageTiles imageTiles);

	/**
	 * @return the minIdGiftTile
	 */
	public int getMinIdGiftTile() {
		return minIdGiftTile;
	}

	/**
	 * @return the maxIdGiftTile
	 */
	public int getMaxIdGiftTile() {
		return maxIdGiftTile;
	}

}
