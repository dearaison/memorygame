package model.factory.giftfactory;

import model.tiles.gifttiles.Gift0;
import model.tiles.gifttiles.Gift1;
import model.tiles.gifttiles.Gifts;
import model.tiles.imagetiles.ImageTiles;

public class GiftFactoryV1 extends GiftFactories {
	public static final GiftFactoryV1 GIFT_FACTORY = new GiftFactoryV1();

	private GiftFactoryV1() {
		this.minIdGiftTile = 0;
		this.maxIdGiftTile = 1;
	}

	public static GiftFactoryV1 getInstance() {
		return GIFT_FACTORY;
	}

	@Override
	public Gifts createGiftTile(int giftTileID, ImageTiles imageTiles) {
		Gifts result = null;
		switch (giftTileID) {
		case 0:
			result = new Gift0(imageTiles);
			break;
		case 1:
			result = new Gift1(imageTiles);
			break;
		default:
			break;
		}
		return result;
	}
}
