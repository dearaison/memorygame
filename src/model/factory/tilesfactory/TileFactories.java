package model.factory.tilesfactory;

import model.tiles.imagetiles.ImageTiles;

public abstract class TileFactories {
	protected int minIdTile;
	protected int maxIdTile;

	public abstract ImageTiles createTile(int tileID);

	/**
	 * @return the minIdTile
	 */
	public int getMinIdTile() {
		return minIdTile;
	}

	/**
	 * @return the maxIdTile
	 */
	public int getMaxIdTile() {
		return maxIdTile;
	}

}
