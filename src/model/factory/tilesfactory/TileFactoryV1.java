package model.factory.tilesfactory;

import model.tiles.imagetiles.*;

public class TileFactoryV1 extends TileFactories {

	public static final TileFactoryV1 TILE_FACTORY = new TileFactoryV1();

	private TileFactoryV1() {
		this.minIdTile = 0;
		this.maxIdTile = 9;
	}

	public static TileFactoryV1 getInstance() {
		return TILE_FACTORY;
	}

	@Override
	public ImageTiles createTile(int tileID) {
		ImageTiles result = null;
		switch (tileID) {
		case 0:
			result = new ImageTile0();
			break;
		case 1:
			result = new ImageTile1();
			break;
		case 2:
			result = new ImageTile2();
			break;
		case 3:
			result = new ImageTile3();
			break;
		case 4:
			result = new ImageTile4();
			break;
		case 5:
			result = new ImageTile5();
			break;
		case 6:
			result = new ImageTile6();
			break;
		case 7:
			result = new ImageTile7();
			break;
		case 8:
			result = new ImageTile8();
			break;
		case 9:
			result = new ImageTile9();
			break;
		default:
			break;
		}
		return result;
	}
}
