package model.adapter;

import model.Model;
import model.tiles.imagetiles.ImageTiles;

public class TilePanelAndGameBoardAdapter implements IAdapter {
	Model model;

	/**
	 * @param model
	 */
	public TilePanelAndGameBoardAdapter(Model model) {
		super();
		this.model = model;
	}

	public ImageTiles getImageTile(int tilePosition) {
		return this.model.getImageTile(tilePosition);
	}

	public int getTilesListSize() {
		return this.model.getTilesListSize();
	}

	public Model getObserableGameBoard() {
		return this.model.getObserableGameBoard();
	}
}