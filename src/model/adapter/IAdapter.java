package model.adapter;

import model.Model;
import model.tiles.imagetiles.ImageTiles;

public interface IAdapter {

	public int getTilesListSize();

	public ImageTiles getImageTile(int tilePosition);

	public Model getObserableGameBoard();
}
