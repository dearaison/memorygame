package model.tiles.imagetiles;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public abstract class ImageTiles {
	public static final int TILE_WIDTH = 120;
	public static final int TILE_HEIGHT = 120;

	protected String name;
	protected boolean visible;
	protected byte score;
	protected byte minusScore;
	protected BufferedImage coverImage;
	protected BufferedImage image;

	public ImageTiles() {
		try {
			coverImage = ImageIO.read(getClass().getResourceAsStream("/images/fond.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @return the minusScore
	 */
	public abstract int getMinusScore();

	/** @return the score */
	public abstract int getScore();

	/**
	 * @return the visible
	 */
	public boolean isVisible() {
		return visible;
	}

	/**
	 * @param visible
	 *            the visible to set
	 */
	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	/**
	 * @return the image
	 */
	public Image getImage() {
		return image;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(ImageTiles that) {
		if (!this.name.equals(that.name)) {
			return false;
		}
		return true;
	}

	public void drawImageTile(Graphics g, int x, int y, JPanel panel) {
		if (isVisible()) {
			g.drawImage(this.image, x, y, panel);
		} else {
			g.drawImage(this.coverImage, x, y, panel);

		}
	}
}
