package model.tiles.imagetiles;

import java.io.IOException;
import javax.imageio.ImageIO;

public class ImageTile2 extends ImageTiles {

	public ImageTile2() {
		this.visible = false;
		this.score = 5;
		this.minusScore = 1;
		this.name = "Image tile 2";

		try {
			this.image = ImageIO.read(getClass().getResourceAsStream("/images/im2.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public int getScore() {
		return this.score;
	}

	@Override
	public int getMinusScore() {
		return this.minusScore;
	}

}
