package model.tiles.imagetiles;

import java.io.IOException;
import javax.imageio.ImageIO;

public class ImageTile9 extends ImageTiles {

	public ImageTile9() {
		this.visible = false;
		this.score = 5;
		this.minusScore = 1;
		this.name = "Image tile 9";

		try {
			this.image = ImageIO.read(getClass().getResourceAsStream("/images/im9.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public int getScore() {
		return this.score;
	}

	@Override
	public int getMinusScore() {
		return this.minusScore;
	}
}
