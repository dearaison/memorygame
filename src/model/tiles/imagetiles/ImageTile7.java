package model.tiles.imagetiles;

import java.io.IOException;
import javax.imageio.ImageIO;

public class ImageTile7 extends ImageTiles {

	public ImageTile7() {
		this.visible = false;
		this.score = 5;
		this.minusScore = 1;
		this.name = "Image tile 7";

		try {
			this.image = ImageIO.read(getClass().getResourceAsStream("/images/im7.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public int getScore() {
		return this.score;
	}

	@Override
	public int getMinusScore() {
		return this.minusScore;
	}
}
