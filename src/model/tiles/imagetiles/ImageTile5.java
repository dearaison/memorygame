package model.tiles.imagetiles;

import java.io.IOException;
import javax.imageio.ImageIO;

public class ImageTile5 extends ImageTiles {

	public ImageTile5() {
		this.visible = false;
		this.score = 5;
		this.minusScore = 1;
		this.name = "Image tile 5";

		try {
			this.image = ImageIO.read(getClass().getResourceAsStream("/images/im5.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public int getScore() {
		return this.score;
	}

	@Override
	public int getMinusScore() {
		return this.minusScore;
	}
}
