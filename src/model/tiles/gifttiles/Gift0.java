package model.tiles.gifttiles;

import java.io.IOException;

import javax.imageio.ImageIO;

import model.tiles.imagetiles.ImageTiles;

public class Gift0 extends Gifts {

	public Gift0(ImageTiles aImageTile) {
		this.giftScore = 1;
		this.aImageTile = aImageTile;
		this.name = "Small gift";
		try {
			this.image = ImageIO.read(getClass().getResourceAsStream("/images/gift.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public int getScore() {
		return aImageTile.getScore() + this.giftScore;
	}

	@Override
	public int getMinusScore() {
		return this.aImageTile.getMinusScore();
	}
}
