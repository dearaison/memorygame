package model.tiles.gifttiles;

import java.io.IOException;

import javax.imageio.ImageIO;

import model.tiles.imagetiles.ImageTiles;

public class Gift1 extends Gifts {
	public Gift1(ImageTiles aImageTile) {
		this.giftScore = 2;
		this.aImageTile = aImageTile;
		this.name = "Normal gift";
		try {
			this.image = ImageIO.read(getClass().getResourceAsStream("/images/gift1.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public int getScore() {
		return aImageTile.getScore() + this.giftScore;
	}

	@Override
	public int getMinusScore() {
		return this.aImageTile.getMinusScore();
	}
}
