package model;

import java.util.Observable;

import model.tiles.imagetiles.ImageTiles;

public abstract class Model extends Observable {
	public abstract int getTilesListSize();

	public abstract ImageTiles getImageTile(int tilePosition);

	public abstract Model getObserableGameBoard();

	public abstract void modifier(int tileZone);

	public abstract void rePlay();

	public abstract int calculateScore();
}
