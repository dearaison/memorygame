package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import model.factory.giftfactory.GiftFactories;
import model.factory.tilesfactory.TileFactories;
import model.tiles.imagetiles.ImageTiles;

public class GameBoard extends Model {
	/* Number of tiles on a row */
	public static final int NUMBER_OF_TILES_ON_ROW = 5;

	/* Number of tiles on a row */
	public static final int NUMBER_OF_TILES_ON_COLUMN = 4;

	/* the list of image tiles. */
	private List<ImageTiles> order;

	/* Tile factory */
	private TileFactories tileFctory;

	/* gift factory */
	private GiftFactories giftFactory;

	/*
	 * each time two image tiles don't match with other. Your score will be
	 * minus 2.
	 */
	private int totalMinusScore;

	/* total score */
	private int totalScore;

	/* first tile was clicked */
	private int tileZone1;

	/* second tile was clicked */
	private int tileZone2;

	/* number of mouse click of one turn */
	private byte numOfClick;

	/**
	 * Constructor
	 */
	public GameBoard(TileFactories tileFactory, GiftFactories giftFactory) {
		super();
		order = new ArrayList<>();
		this.tileFctory = tileFactory;
		this.giftFactory = giftFactory;
		for (int i = this.tileFctory.getMinIdTile(); i <= this.tileFctory.getMaxIdTile(); i++) {
			order.add(tileFactory.createTile(i));
			order.add(tileFactory.createTile(i));
		}

		applyRandomGift();

		Collections.shuffle(order);

		totalMinusScore = 0;
		totalScore = 0;
		tileZone1 = -1;
		tileZone2 = -2;
		numOfClick = 0;
	}

	/**
	 * This method will create two random number as two index in tiles list.
	 * They are different from another. And the second number is not the index
	 * of the tile whose type is same as the tile at first index.
	 */
	private ArrayList<Integer> randomGiftPosition() {
		ArrayList<Integer> result = new ArrayList<>();
		Random randomObj = new Random();
		for (int i = this.giftFactory.getMinIdGiftTile(); i <= this.giftFactory.getMaxIdGiftTile(); i++) {
			Integer ranNum = randomObj.nextInt(order.size());
			while (result.contains(ranNum)) {
				ranNum = randomObj.nextInt(order.size());
			}
			result.add(ranNum);
		}
		return result;
	}

	/**
	 * Base on the result form randomGiftPosition method, we decorate the tile
	 * at the gift index with a gift tile.
	 */
	private void applyRandomGift() {
		ArrayList<Integer> randomList = randomGiftPosition();
		for (int i = 0, giftID = this.giftFactory.getMinIdGiftTile(); i < randomList.size()
				&& giftID <= this.giftFactory.getMaxIdGiftTile(); i++, giftID++) {
			int randomPosition = randomList.get(i);
			ImageTiles imageTiles = order.get(randomPosition);

			for (int j = 0; j < order.size(); j++) {
				if (order.get(j).equals(imageTiles)) {
					order.set(j, this.giftFactory.createGiftTile(giftID, order.get(j)));
				}
			}
		}

	}

	/** @return the size of tiles list */
	public int getTilesListSize() {
		return this.order.size();
	}

	/**
	 * @return the a tile in order
	 */
	public ImageTiles getImageTile(int tilePosition) {
		return order.get(tilePosition);
	}

	/**
	 * When a tile was click that tile will be visible. IF two visible tile is
	 * not same, those tiles will be invisible again, the score will be minus 2.
	 * If two visible tile is same, those tiles will be visible until end of
	 * game, the score will be plus by the score of two tiles.
	 */
	public void modifier(int tileZone) {
		if (numOfClick == 2) {
			numOfClick = 0;
			if (!order.get(tileZone1).equals(order.get(tileZone2))) {
				totalMinusScore += order.get(tileZone1).getMinusScore() + order.get(tileZone2).getMinusScore();
				order.get(tileZone1).setVisible(false);
				order.get(tileZone2).setVisible(false);
			}
		}

		if (!order.get(tileZone).isVisible()) {
			if (numOfClick == 0) {
				tileZone1 = tileZone;
				numOfClick++;
				order.get(tileZone1).setVisible(true);
			} else if (numOfClick == 1) {
				tileZone2 = tileZone;
				numOfClick++;
				order.get(tileZone2).setVisible(true);
				if (order.get(tileZone1).equals(order.get(tileZone2))) {
					totalScore += order.get(tileZone1).getScore() + order.get(tileZone2).getScore();
				}
			}
			this.setChanged();
			this.notifyObservers();
		}
	}

	/**
	 * calculate player score.
	 */
	public int calculateScore() {
		return this.totalScore - totalMinusScore;
	}

	/**
	 * reset game
	 */
	public void rePlay() {
		order.clear();
		for (int i = this.tileFctory.getMinIdTile(); i <= this.tileFctory.getMaxIdTile(); i++) {
			order.add(tileFctory.createTile(i));
			order.add(tileFctory.createTile(i));
		}

		applyRandomGift();

		Collections.shuffle(order);

		totalMinusScore = 0;
		totalScore = 0;
		tileZone1 = -1;
		tileZone2 = -2;
		numOfClick = 0;
		this.setChanged();
		this.notifyObservers();
	}

	@Override
	public Model getObserableGameBoard() {
		return this;
	}

}
