package main;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import controller.GameBoardController;
import controller.IController;
import controller.ReplayController;
import controller.ScoreController;
import model.GameBoard;
import model.Model;
import model.adapter.IAdapter;
import model.adapter.TilePanelAndGameBoardAdapter;
import model.factory.giftfactory.GiftFactories;
import model.factory.giftfactory.GiftFactoryV1;
import model.factory.tilesfactory.TileFactories;
import model.factory.tilesfactory.TileFactoryV1;
import view.GameFrame;
import view.ReplayingButton;
import view.ScoreLable;
import view.TilesPanel;

public class GameStarter {
	public static void main(String[] args) {
		TileFactories factory = TileFactoryV1.getInstance();
		GiftFactories factories = GiftFactoryV1.getInstance();
		Model model = new GameBoard(factory, factories);

		IController gameBoardController = new GameBoardController(model);
		IController replayController = new ReplayController(model);
		IController scController = new ScoreController(model);

		IAdapter adapter = new TilePanelAndGameBoardAdapter(model);

		JPanel tilesPanle = new TilesPanel(adapter, gameBoardController);
		JLabel scoreLaybel = new ScoreLable(scController, adapter);
		JButton replayButton = new ReplayingButton(replayController);

		new GameFrame(tilesPanle, scoreLaybel, replayButton);
	}
}
